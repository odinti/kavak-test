// Update with your config settings.

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

module.exports = {

    development: {
        client: 'mysql',
        connection: {
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD,
            database: process.env.MYSQL_DB,
        },
        migrations: {
            tableName: 'knex_migrations'
        }
    }

};