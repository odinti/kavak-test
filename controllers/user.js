const { User } = require('../helpers/bookshelf');

class UserController {
    static async index(req, res) {
        try {
            // create object
            const user = await User.fetchAll()
            if (user) {
                res.json(
                    user.serialize()
                )
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async show(req, res) {
        try {
            // create object
            const user = await new User({ id: req.params.id }).fetch({
                require: false
            })
            if (user) {
                res.status(200).json(
                    user.serialize()
                )
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }
}

module.exports = UserController;