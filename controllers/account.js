const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const { validationResult } = require('express-validator');

const { User } = require('../helpers/bookshelf');

class AccountController {
    static async index(req, res) {
        res.json(req.user)
    }


    static async register(req, res) {
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }

        try {
            // assert unique email

            const exists = await User.where('email', '=', req.body.email).count()

            if (exists) {
                res.status(422).json({ error: "Email already in use" })
                return;
            }

            const user = new User({
                email: req.body.email,
                name: req.body.name,
                password: bcrypt.hashSync(req.body.password, 10)
            });

            await user.save();

            res.status(201).json({
                token: jwt.sign(user.serialize(), process.env.JWT_SECRET)
            })
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async login(req, res) {
        const errors = validationResult(req)
        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }
        // get email
        // get password
        // compare with db
        // return JWT
        try {
            // assert unique email

            const user = await User.where('email', '=', req.body.email).fetch({
                require: false
            })

            if (!user) {
                res.status(422).json({ error: "Credentials does not match" })
                return;
            }

            const match = bcrypt.compareSync(req.body.password, user.get('password'))

            if (!match) {
                res.status(422).json({ error: "Credentials does not match" })
                return;
            }


            res.json({
                token: jwt.sign(user.serialize(), process.env.JWT_SECRET)
            })
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }
}

module.exports = AccountController;