class TestController {
    static test(req, res) {

        const arr = req.body.array.map(x => parseInt(x)).sort()

        const target = parseInt(req.body.target)

        let exists = arr.indexOf(target)

        let newArray = [...arr]

        let closestIndex = -1

        let closestValue = Infinity

        let closestDelta = Infinity

        if (exists === -1) {
            arr.forEach((value, index) => {
                const delta = target - value
                if (delta < 0) {
                    return
                }
                if (delta < closestDelta) {
                    closestIndex = index
                    closestValue = value
                    closestDelta = delta
                }
            })

            if (closestIndex === -1) {
                newArray.unshift(target)
                exists = 0
            } else {
                newArray.splice(closestIndex + 1, 0, target)
                exists = newArray.indexOf(target)
            }

        }

        console.log({
            original: arr,
            emulated: newArray,
            index: exists,
            target: target,
            closestIndex,
            closestValue,
            closestDelta
        })

        res.json({
            index: exists,
        })
    }
}

module.exports = TestController;