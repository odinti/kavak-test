const { Task, File } = require('../helpers/bookshelf');
const { validationResult } = require('express-validator');
const path = require('path');
const crypto = require("crypto");
const fs = require('fs').promises;

class TaskController {
    static async index(req, res) {
        try {

            let query = Task.query((q) => {
                if ('completed' in req.query) {
                    q = q.where('completed_at', 'IS NOT', null)
                }

                if ('user_id' in req.query) {
                    q = q.where('user_id', '=', req.query.user_id)
                }

                if ('from:created' in req.query) {
                    q = q.where('created_at', '>=', req.query['from:created'])
                }

                if ('to:created' in req.query) {
                    q = q.where('created_at', '<=', req.query['to:created'])
                }

                if ('from:completed' in req.query) {
                    q = q.where('completed_at', '>=', req.query['from:completed'])
                }

                if ('to:completed' in req.query) {
                    q = q.where('completed_at', '<=', req.query['to:completed'])
                }

                if ('sort' in req.query) {
                    const direction = 'sort_dir' in req.query ? req.query.sort_dir : 'ASC'
                    q = q.orderBy(req.query.sort, direction)
                }
            });

            const tasks = await query.fetchAll();

            await tasks.load('file')

            res.json(tasks);
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async show(req, res) {
        try {
            // create object
            const task = await new Task({ id: req.params.id }).fetch({
                require: false
            })
            if (task) {
                res.status(200).json(
                    task.serialize()
                )
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async store(req, res) {
        const errors = validationResult(req)

        if (!errors.isEmpty()) {
            return res.status(422).json({ errors: errors.array() })
        }

        try {
            // create object
            const task = new Task({
                name: req.body.name,
                user_id: req.user.id,
                expires_at: req.body.expires_at
            });

            await task.save();

            if (req.files.file) {
                const name = crypto.randomBytes(20).toString('hex');
                const pieces = req.files.file.name.split('.')
                const extension = pieces[pieces.length - 1]
                const storagePath = path.resolve(__dirname, '../storage', `${name}.${extension}`);
                req.files.file.mv(storagePath, async function(err) {
                    if (err) return res.status(500).send(err);

                    const file = new File({
                        name: storagePath,
                        task_id: task.id
                    })

                    await file.save();

                    await task.load('file');

                    res.status(201).json(task.serialize())
                });
            } else {
                res.status(201).json(task.serialize())
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async update(req, res) {
        try {
            // create object
            const task = await new Task({ id: req.params.id }).fetch({
                require: false
            });
            if (task) {
                await task.save({
                    name: req.body.name,
                    expires_at: req.body.expires_at
                });
                res.status(200).json(
                    task.serialize()
                )
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async destroy(req, res) {
        try {
            // create object
            const task = await new Task({ id: req.params.id }).fetch({
                require: false,
                withRelated: 'file'
            })
            if (task) {
                if (task.related('file')) {
                    try {
                        await fs.unlink(task.related('file').get('name'))
                        await task.related('file').destroy()
                    } catch (e) {
                        //
                    }
                }
                await task.destroy()
                res.status(204).send()
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }
}

module.exports = TaskController;