const moment = require('moment')
const { Task } = require('../helpers/bookshelf');

class CompletedTaskController {

    static async set(req, res) {
        try {
            // create object
            const task = await new Task({ id: req.params.id }).fetch({
                require: false
            });
            if (task) {
                await task.save({
                    completed_at: moment.utc().format('YYYY-MM-DD HH:mm:ss')
                });
                res.status(201).json(
                    task.serialize()
                )
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }

    static async destroy(req, res) {
        try {
            // create object
            const task = await new Task({ id: req.params.id }).fetch({
                require: false
            });
            if (task) {
                await task.save({
                    completed_at: null
                });
                res.status(204).send()
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }
}

module.exports = CompletedTaskController;