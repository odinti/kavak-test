const { File } = require('../helpers/bookshelf');

class FileController {
    static async show(req, res) {
        try {
            // create object
            const file = await new File({ id: req.params.id }).fetch({
                require: false
            });
            if (file) {
                res.download(file.get('name'))
            } else {
                res.status(404).send()
            }
        } catch (e) {
            res.status(503).json({ error: e.message })
        }
    }
}

module.exports = FileController;