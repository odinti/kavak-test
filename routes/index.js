const jwt = require('express-jwt');

module.exports = function(app) {
    app.use('/api/v1', require('./guest'));
    app.use('/api/v1', [jwt({ secret: process.env.JWT_SECRET })], require('./authenticated'));
};