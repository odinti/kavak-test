const express = require('express');
const AccountController = require('../controllers/account');
const TestController = require('../controllers/test');
const guest = express.Router();

const { body } = require('express-validator');


guest
    .post('/account/register', [
        body('name').not().isEmpty(),
        body('email').isEmail().normalizeEmail(),
        body('password').isLength({ min: 8 })
    ], AccountController.register)
    .post('/account/login', [
        body('email').isEmail().normalizeEmail(),
        body('password').isLength({ min: 8 })
    ], AccountController.login)
    .post('/test', [
        body('array').isArray(),
        body('target').isNumeric()
    ], TestController.test)

module.exports = guest;