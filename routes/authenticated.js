const express = require('express');
const AccountController = require('../controllers/account');
const UserController = require('../controllers/user');
const TaskController = require('../controllers/task');
const FileController = require('../controllers/file');
const CompletedTaskController = require('../controllers/completed-task');
const authenticated = express.Router();
const { body, query } = require('express-validator');


authenticated
    .get('/account/me', AccountController.index)

authenticated
    .get('/users', UserController.index)
    .get('/users/:id', UserController.show)

authenticated
    .get('/files/:id', FileController.show)

authenticated
    .get('/tasks', [
        query('completed').isBoolean().optional(),
        query('user_id').isNumeric().optional(),
        query('from:created').isISO8601().optional(),
        query('to:created').isISO8601().optional(),
        query('from:completed').isISO8601().optional(),
        query('to:completed').isISO8601().optional(),
        query('sort').isIn(['created_at', 'completed_at', 'name']).optional(),
        query('sort_dir').isIn(['ASC', 'DESC']).optional(),
    ], TaskController.index)
    .get('/tasks/:id', TaskController.show)
    .patch('/tasks/:id', [
        body('name').not().isEmpty(),
        body('expires_at').isISO8601().optional(),
    ], TaskController.update)
    .delete('/tasks/:id', TaskController.destroy)
    .post('/tasks', [
        body('name').not().isEmpty(),
        body('expires_at').isISO8601().optional(),
    ], TaskController.store)

authenticated
    .put('/completed-tasks/:id', CompletedTaskController.set)
    .delete('/completed-tasks/:id', CompletedTaskController.destroy)

module.exports = authenticated;