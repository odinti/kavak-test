if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config()

}

const express = require('express');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }))

app.use(bodyParser.json())

// 10 MB limit
app.use(fileUpload({
    limits: { fileSize: 10 * 1024 * 1024 },
}));

require('./routes/index.js')(app);

app.listen(3000, function() {
    console.log('Server online');
})