const knex = require('knex')({
    client: 'mysql',
    connection: {
        host: process.env.MYSQL_HOST,
        user: process.env.MYSQL_USER,
        password: process.env.MYSQL_PASSWORD,
        database: process.env.MYSQL_DB,
        charset: 'utf8'
    }
})


const bookshelf = require('bookshelf')(knex)

bookshelf.plugin('bookshelf-virtuals-plugin')

const User = bookshelf.model('User', {
    tableName: 'users',
    hidden: ['password'],
    tasks() {
        return this.hasMany(Task)
    }
});

const File = bookshelf.model('File', {
    tableName: 'files',
    hidden: ['name', 'task_id'],
    task() {
        return this.belongsTo(Task)
    },
    virtuals: {
        url() {
            return this.get('id') ? `/api/v1/files/${this.get('id')}` : null
        }
    }
});


const Task = bookshelf.model('Task', {
    tableName: 'tasks',
    user() {
        return this.belongsTo(User)
    },
    file() {
        return this.hasOne(File)
    }
})

module.exports = {
    User,
    File,
    Task
};